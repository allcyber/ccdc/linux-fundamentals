# Searching For Files

Tools for finding certain files on the system.

## `find` Command

#### TL;DR

> find \[search root] \[search filter] \[actions]

#### Description

The `find` commnad recursively searches a directory for any files and/or directories where the search expression is true. Searching starts in the current directory unless another directory is specified. In addition to returning a list of matching results, `find` can also perform actions on all matching items. There are **many** different search filters and actions. Some useful ones are listed below, but view the `man` pages for a full list.

Search operators, and actions, can be combined to create complex search queries. Search filters can be used multiple times to filter the results more specifically. 

`find` also has operators to modify how expressions are evaluated.

##### Useful Operators

- and
  - > \<expression1> -a \<expression2>
  - both expressions must be true. `and` is implied when 2 expressions are next to each other, so its usually unecessary.
  
- or
  - > \<expression1> -o \<expression2>
  - either expression can be true.

- not
  - > -not \<expression>
  - > ! \<expression>
  - negates an expression. True becomes false, false becomes true

##### Useful Filters

- name
  - > -name \<name>
  - items with `name` in the item's name.

- user
  - > -user \<user>
  - items owned by `user`. You can use the user name or uid.
  
- group
  - > -group \<group>
  - items owned by `group`. You can use the group name or gid.

- amin
  - > -amin \[+/-]\<minutes>
  - items accessed `minutes` ago. `minutes` can take a prefix of `+` for more than, or `-` for less than.

- mmin
  - > -mmin \[+/-]\<minutes>
  - items modified `minutes` ago. `minutes` can take a prefix of `+` for greater than, or `-` for less than.

- size
  - > -size \[+/-]\<size>\[unit]
  - items that are `size` in size. `size` can take a prefix of `+` for greater than, or `-` for less than. `size` can also take a suffix of `unit` to specifiy the units of `size`. A few available units are `c` for bytes, `k` for KiB, `M` for MiB, and `G` for GiB.

- type
  - > -type \<types>
  - items that are of type `types`. A few available types are `d` for directory, `f` for file, and `s` for socket. Multiple types can be specified at once by seperating each type with a `,`.

- perm
  - > -perm \[- or /]\<permissions>
  - items with permissions matching `permissions`. `permissions` can be in the same forms that the `chmod` command uses, with octal numbers or symbolic characters. Multiple permissions can be searched at once. `permissions` can take a prefix of `-` which is true if all of an item's permissions are in `permissions`, or `/` which is true if any part of an item's set permissions are in `permissions`.

##### Useful Actions

- ls
  - > -ls
  - lists matching items like `ls -ldis` would.

- delete
  - > -delete
  - delete matching files
  
- exec
  - > -exec \<command> \[+]
  - execue a command once for each item found. `command` needs to be ended with `\;`. The string `'{}'` will be replaced with the item's filename when the command is run. If the `+` option is added instead of executing `command` once for each item found, it appends each filename and runs `command` in batches similar to the `xargs` command.

- execdir
  - > -execdir \<command> \[+]
  - extectly the same as `-exec`, but it runs `command` in the directory where the item is found. 

##### Examples

    # find and list all .ssh directories on the system
    # the 2>/dev/null removes error messages about stuff we can't read
    user@host:$ find / -type d -name .ssh -ls 2>/dev/null
    1311066      4 drwx------   2 rusty    rusty        4096 Nov 13 03:49 /home/rusty/.ssh

<br/>

    # find all .zip files under the current diretory, owned by user austin, and unzip them in the directory they are found
    user@host:$ find -name '*.zip' -user austin -execdir unzip '{}' \;
    Archive:  ./files.zip
      inflating: home/austin/Downloads/3317550.3321429.pdf  
      inflating: home/austin/Downloads/allcyber_logo.png  
      inflating: home/austin/Downloads/appimagelauncher-2.2.0-travis1012.64a086e.x86_64.rpm  
      inflating: home/austin/Downloads/BD84B67F-A647-45B2-A89F-468B34ABCBED.mov  
      inflating: home/austin/Downloads/IMG-1821.JPG  
        creating: home/austin/Downloads/ISOs/
    Archive:  ./stuff.zip
        creating: stuff/
      inflating: stuff/transfer_log.json  
      inflating: stuff/stats.py          
      inflating: stuff/out.txt           
      inflating: stuff/bytes.txt         
      inflating: stuff/authentication_log.json  
      inflating: stuff/add.py            

<br/>

    # find all files and directories in ~/coolstuff, that have been modified in the last 10 to 15 minutes
    user@host:$ find ~/coolstuff -mmin +10 -mmin -15
    /home/niki/coolstuff
    /home/niki/coolstuff/things
    /home/niki/coolstuff/things/secrets.pdf
    /home/niki/coolstuff/things/plans/WorldDomination.md
    /home/niki/coolstuff/things/plans/Movies2Watch.ods
    /home/niki/coolstuff/things/memes.zip

<br/>

    # find all files under /somedir that end in .idk and delete them
    user@host:$ find /somedir -type f -name '*.idk' -delete

----------------------------------------------------------------------------------------------------------

## `locate` Command

#### TL;DR

> locate \<search term>

#### Description

The `locate` commnad displays a list of file paths on the system that contain `search term`. Unlike `find` it does not have tons of search filters, and it does not search through directories manually. It uses a database to keep track of all the files on the system and just searches the database. It can get results very fast, but will not find anything that is not in its database. The databse updates periodically on its own, but can be manually updated with the `updatedb` command.

`locate` does have few options (check out the `man` page), but its a pretty simple command.

##### Useful Options

- regexp
  - > -r / --regexp
  - > --regex
  - Allows for the use of regular expression for searching. `-r` and `--regexp` can be used multiple times and use basic regular expressions. `--regex` can be used with mutiple search terms at once and uses extended regular expressions.

##### Examples

    user@host:$ locate .ssh
    /home/jordan/.ssh
    /home/jordan/.ssh/id_rsa
    /home/jordan/.ssh/id_rsa.pub
    /home/jordan/.ssh/known_hosts
    /usr/sbin/mount.fuse.sshfs
    /usr/sbin/mount.sshfs
    /usr/share/doc/openssh/PROTOCOL.sshsig
    /usr/share/mime/inode/vnd.kde.service.ssh.xml
    /var/lib/snapd/snap/core18/1885/lib/systemd/system/snapd.sshd-keygen.service
    /var/lib/snapd/snap/core18/1885/lib/systemd/system/multi-user.target.wants/snapd.sshd-keygen.service
    /var/lib/snapd/snap/core18/1932/lib/systemd/system/snapd.sshd-keygen.service
    /var/lib/snapd/snap/core18/1932/lib/systemd/system/multi-user.target.wants/snapd.sshd-keygen.service

<br/>

    user@host:$ locate -r '/Downloads/.*iso'
    /home/daniel/Downloads/ISOs/Parrot-kde-security-4.10_amd64.iso
    /home/daniel/Downloads/ISOs/Win10_2004_English_x64.iso
    /home/daniel/Downloads/ISOs/debian-10.6.0-amd64-netinst.iso
    /home/daniel/Downloads/ISOs/endeavouros-2020.05.08-x86_64.iso
    /home/daniel/Downloads/ISOs/kali-linux-2020.2-installer-amd64.iso
    /home/daniel/Downloads/ISOs/kali-linux-2020.2-installer-netinst-amd64.iso
    /home/daniel/Downloads/ISOs/kali-linux-2020.3-installer-amd64.iso
    /home/daniel/Downloads/ISOs/openSUSE-Leap-15.2-DVD-x86_64.iso
    /home/daniel/Downloads/ISOs/openSUSE-Tumbleweed-DVD-x86_64-Snapshot20200919-Media.iso
    /home/daniel/Downloads/ISOs/ubuntu-20.04.1-desktop-amd64.iso
    /home/daniel/Downloads/ISOs/void-live-x86_64-20191109-cinnamon.iso
    /home/daniel/Downloads/ISOs/void-live-x86_64-20191109-lxqt.iso
    /home/daniel/Downloads/ISOs/void-live-x86_64-20191109.iso
    /home/daniel/Downloads/ISOs/windows-server-2008-r2.iso
    /home/daniel/Downloads/ISOs/windows-server-2012-r2.iso
    /home/daniel/Downloads/ISOs/windows-server-2016.iso
    /home/daniel/Downloads/ISOs/windows-server-2019.iso

----------------------------------------------------------------------------------------------------------

## `lsof` Command

#### TL;DR

> lsof \[options]

#### Description

`lsof` lists open files and the process that opened them. In linux everything is a file. So while it lists open files and directories, it also lists things like open network connections because in linux they are represented as files.

##### Useful Options

FYI, some options can be NOTed using the `^` character. See the `man` pages for more info.

- a
  - > -a
  - AND options together. By default options are ORed together, so file that match either option will be shown. With `-a` files must match all options to be shown.

- p
  - > -p \<process id>
  - files opened by the process with `process id`

- u
  - > -u \<user>
  - files opened by processes owned by `user`. Multiple users can be listed if seperated by `,`s. `user` can be a username or UID number

- i
  - > -i \[ipv4/ipv6]\[protocol]\[@hostname/host address]\[:service/port]
  - files with an internet address. You can filter results by ipv4/ipv6, protocol, host, and/or port. `ipv4/ipv6` can be 4 or 6. `protocol` can be TCP or UDP. `@hostname/host address` can be a hostname/ip address, you have to put a `@` at the start. Also it will match either the local or remote host. `:service/port` can be a number port or a service name for a port number, you have to put a `:` at the start. All these options are optional, but when used they go together without spaces like `4TCP@localhost:22`

- D
  - > <+/->D \<directory>
  - exclude files in or include only files in `directory`. `+` includes and `-` excludes

- n
  - > -n
  - show ip addresses, not hostnames

- P
  - > -P
  - show port numbers, not service names

##### Examples

    # list all open files opened by processes owned by user jessica
    user@host:$ lsof -u jessica
    COMMAND     PID    USER   FD      TYPE             DEVICE  SIZE/OFF       NODE NAME
    systemd    8228 jessica  cwd       DIR              253,1      4096          2 /
    systemd    8228 jessica  rtd       DIR              253,1      4096          2 /
    systemd    8228 jessica  txt       REG              253,1   1730640    2506726 /usr/lib/systemd/systemd
    systemd    8228 jessica  mem       REG              253,1   1917776    2493356 /usr/lib64/libm-2.31.so
    systemd    8228 jessica  mem       REG              253,1    171152    2494116 /usr/lib64/libudev.so.1.6.17
    systemd    8228 jessica  mem       REG              253,1     41096    2504824 /usr/lib64/libffi.so.6.0.2
    systemd    8228 jessica  mem       REG              253,1    324184    2505256 /usr/lib64/libpcap.so.1.9.1
    systemd    8228 jessica  mem       REG              253,1   1592736    2505466 /usr/lib64/libunistring.so.2.1.0
    systemd    8228 jessica  mem       REG              253,1    146992    2504895 /usr/lib64/libgpg-error.so.0.27.0
    ...

<br/>

    # list all open files using ipv6 and TCP
    user@host:$ lsof -i 6TCP
    COMMAND     PID  USER   FD   TYPE  DEVICE SIZE/OFF NODE NAME
    kdeconnec  8476 eipox   13u  IPv6   59346      0t0  TCP *:xmsg (LISTEN)
    ktorrent  32897 eipox   27u  IPv6  518346      0t0  TCP *:6881 (LISTEN)
    nc        69523 eipox    3u  IPv6 1205028      0t0  TCP *:search-agent (LISTEN)

<br/>

    # list all open files using ports between 1024-3000 and is not process 8476, showing port number and ip addresses
    user@host:$ lsof -p '^8476' -a -i :1024-3000 -nP 
    COMMAND   PID  USER   FD   TYPE  DEVICE SIZE/OFF NODE NAME
    nc      69523 tyler    3u  IPv6 1205028      0t0  TCP *:1234 (LISTEN)
    nc      69523 tyler    4u  IPv4 1205029      0t0  TCP *:1234 (LISTEN)
    nc      69851 tyler    3u  IPv4 1202571      0t0  TCP 127.0.0.1:2222 (LISTEN)
    nc      72134 tyler    3u  IPv6 1233308      0t0  UDP *:1025 
    nc      72134 tyler    4u  IPv4 1233309      0t0  UDP *:1025
