# Using basic file system tools

Commands for displaying current directory within the system.

## `pwd` Command

#### TL;DR

> pwd 

> pwd  \[option] 

#### Description

The `pwd` commnad will print the current working directory. very useful when you want to figure out where you are in the filesystem.

Some useful switches are:

1. -L,  --logical use PWD from environment, even if it contains symlinks
2. -P, --physical avoid all symlinks

In context the `-L` option will show the full path including the syslink if any. The `-P` will only show the true working directory without the symlink.

##### Examples

    user@host:$ pwd
    /tmp

    Working with symlinks

    user@host:$ pwd -L
    /home/user/test_symlink/tmp

    user@host:$ pwd -P
    /tmp


## `ls` Command

#### TL;DR

> ls \<options\>
 
> ls \<options\> /Directory

#### Description

The `ls` command is use to list information about FILES in a directory.

Some useful switches are:

1. -a,  --all do not ignore entries starting with .
2. -l,    use a long listing format
3. -h, with -l and -s, print sizes like 1K 234M 2G etc.
4. -R, --reverse order while sorting
5. -H, follow symbolic links listed on the command line
6. -Z, --context print any security context of each file


##### Examples

    user@host:$ ls
    /tmp

    user@host:$ ls -al
    total 120
    drwxrwxrwt 15 root       root       24576 Nov 14 10:57 ./
    drwxr-xr-x 20 root       root       36864 Nov 14 00:15 ../
    drwxrwxrwt  2 root       root        4096 Nov 14 10:24 .font-unix/
    
    user@host:$ ls -h

