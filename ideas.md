# Ideas for cheat sheet

---
# **Tools** 

---
### **0_Help**

	man 
	help
	info
	whatis
## **1_File System**
- ### *00_File Basics*
		pwd
		ls
		cd
		echo
		touch
		mv
		cp
		mkdir
		rm
		rmdir

- ### *01_File Search*
		
		find
		locate
		lsof
		
- ### *02_File Content*
		grep
		head
		tail
		more
		less
		cut
		awk
		sed
		tr
		sort
		uniq
		rev

- ### *03_File permissions*

		chmod
		chown
		umask
		getfacl
		setfacl

- ### *04_Advance File permissions*
		setuid
		groupuid
		lsattr
		chattr
		getcap
		setcap	
		

- ### *05_File Utilitys*

		df
		dd
		mkfs
		mount
		umount
		lsblk
		fdisk
		parted


## **2_Manage Users and groups**

	w
	who
	useradd
	usermod
	groupadd
	groupmod
	passwd
	gpasswd


	

## **3_Managing processes**

	which
	ps
	top
	kill
	pkill
	ls /proc/$pid/fd/ | wc -l
	fuser
## **4_Services and Jobs**

	systemctl
	fg
	service
	cron
	at
	


## **5_Networks**	


- ### **00_Managing Networks**

		ifconfig
		ip
		ping
		arp
		ss
		route
		traceroute
		dhclient
		nmcli


- ### **01_Monitoring Networks**
  		netstat
		tcpdump
		tshark

- ### **02_swissArmy Networks**
		wget
		curl
		nmap
		nc
		nslookup
		dig	
- ### **03_Firewall**

		ufw
		firewall-cmd
		iptable
		nftables




