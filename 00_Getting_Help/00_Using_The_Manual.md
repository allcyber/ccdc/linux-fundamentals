# Getting Help

Commands for getting help with the system.

## `man` Command

#### TL;DR

> man \<command\>

> man \<page #\> \<command\>

#### Description

The `man` commnad will display the system reference manual for commands, config files, and more.

Manuals have different pages for different things. The pages are...

1. Executable programs or shell commands
2. System calls (functions provided by the kernel)
3. Library calls (functions within program libraries)
4. Special files (usually found in /dev)
5. File formats and conventions, e.g. /etc/passwd
6. Games
7. Miscellaneous (including macro packages and conventions), e.g. man(7), groff(7)
8. System administration commands (usually only for root)
9. Kernel routines [Non standard]

You would use `man <search term>` to view the manual for something.

You can also specify a page by using `man <page #> <search term>`

##### Examples

    user@host:$ man man

    NAME
       man - an interface to the system reference manuals

    SYNOPSIS
       man [man options] [[section] page ...] ...
    ...

<br/>

    user@host:$ man 5 passwd

    NAME
       passwd - password file

    DESCRIPTION
       The /etc/passwd file is a text file that describes user login accounts for the system.  It should 
    ...

## `-h` / `--help` Command arguments

#### TL;DR

> \<command\> -h

> \<command\> --help

> \<command\> help

#### Description

Many commands have help arguments or subcommands to descibe how to use them.

##### Examples

    user@host:$ cat --help
    
    Usage: cat [OPTION]... [FILE]...
    Concatenate FILE(s) to standard output.

    With no FILE, or when FILE is -, read standard input.

    -A, --show-all           equivalent to -vET
    ...

<br/>

    user@host:$ ss -h

    Usage: ss [ OPTIONS ]
       ss [ OPTIONS ] [ FILTER ]
       -h, --help          this message
       -V, --version       output version information
    ...

<br/>

    user@host:$ go help

    Go is a tool for managing Go source code.

    Usage:

        go <command> [arguments]

    The commands are:

        bug         start a bug report
    ...

## `info` Command

#### TL;DR

> info \<command\>

#### Description

The `info` command is an alternative to the `man` command. `info` has its own database of information, which usually has longer, more detailed descriptions than `man`.

##### Examples

    user@host:$ info ss
    
    NAME
       ss - another utility to investigate sockets

    SYNOPSIS
       ss [options] [ FILTER ]

    DESCRIPTION
       ss is used to dump socket statistics. It allows showing information similar to netstat.  It can display more TCP and state information than other tools.
    ...

## `whatis` Command

#### TL;DR

> whatis \<search term\>

#### Description

The `whatis` command gives a brief decription of a commands or config files, and shows the available `man` pages for them. It searches `man` pages for names that have the search term.

##### Examples

    user@host:$ whatis whatis

    whatis (1)           - display one-line manual page descriptions

## `apropos` Command

#### TL;DR

> apropos \<search term\>

#### Description

The `apropos` command is for searching through `man` pages. It is similar to `whatis`, but this command shows brief descriptions for all commands with a name or description containing the search term.

##### Examples

    user@host:$ apropos 'copy files'

    cp (1)               - copy files and directories
    cp (1p)              - copy files
    cpio (1)             - copy files to and from archives
    docker-cp (1)        - Copy files/folders between a container and the local filesystem
    File::Copy (3pm)     - Copy files or filehandles
    ...